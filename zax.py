#Para clonar repositorio: https://gitlab.com/felipe_carneiro/desafio-encaminhado-pelas-empresas.git

from time import sleep

print("""Moto 1 - cobra R$2 reais por entrega e atende todas as lojas
Moto 2 - cobra R$2 reais por entrega e atende todas as lojas
Moto 3 - cobra R$2 reais por entrega e atende todas as lojas
Moto 4 - cobra R$2 reais por entrega e atende apenas a loja 1
Moto 5 - cobra R$3 reais por entrega e atende todas as lojas
""")
sleep(1)
print("""O Moto 1 atende todas as lojas
O Moto 2 atende todas as lojas
O Moto 3 atende todas as lojas
O Moto 4 atende apenas a loja 1
O Moto 5 atende todas as lojas
""")
sleep(1)
print("""Loja 1 - 3 pedidos (PEDIDO 1 R$50, PEDIDO 2 R$50, PEDIDO 3 R$50) e paga 5% do valor pedido por entrega fora o valor fixo. 
Loja 2 - 4 pedidos (PEDIDO 1 R$50, PEDIDO 2 R$50, PEDIDO 3 R$50, PEDIDO 4 R$50) e paga 5% do valor pedido por entrega fora o valor fixo.
Loja 3 - 3 pedidos (PEDIDO 1 R$50, PEDIDO 2 R$50, PEDIDO 3 R$100) e paga 15% do valor pedido por entrega fora o valor fixo.""")
sleep(2)

entrega = [1,2,3,4,5]
estabelecimento = [1,2,3,4]
loja1,loja2,loja3,loja4 = estabelecimento
moto1,moto2,moto3,moto4,moto5 = entrega

l1 = 1
l2 = 1
l3 = 1

# porcentagem que cada loja paga por entrega aos motoboys
p_loja1 = 5
p_loja2 = 5
p_loja3 = 15

#a variavel significa os valores que normalmente a loja cobra pelas entregas
valor = 50
valor1 = 100

#valor que cada motoboy cobra por cada entrega feita
dinheiro_por_entrega = 2

# faz o calculo ja do total de todas as entregas, mais com o valor fixo de cada motoboy cobra de 2 R$
#d_moto significa o dinheiro do motoboy
d_moto1= 20 + 4
d_moto2= 20 + 4
d_moto3= 20 + 4
d_moto4= 15 + 6
d_moto5= 5 + 2

print("\n Selecione qual loja deseja verificar ")
print('''\n\033[1;31mSELECIONE A LOJA:\033[m
[1] Loja 1
[2] Loja 2
[3] Loja 3
[4] verificar as entregas de todas as lojas''')

#selecionando as opções de 1 a 4 ele irá apresentar uma loja especifica ou mostrar todas 
mot = input("escolha:")

if mot == '1':    
    print("\n LOJA 1 \n")
    while l1 < 4:
            print("motoboy {} pegou a {} entrega da loja 1".format(moto4,l1))
            sleep(2)
            l1 = l1+1
    print("faturou" ,  (p_loja3 % d_moto1) ,"R$ pelas 3 entregas"  )
    print("Entregas realizadas")

elif mot == '2':
    print("\n LOJA 2 \n")  
    while l2 < 5:
        if (l2 == 1):
            print("motoboy {} pegou a {} entrega da loja 2 ".format(moto1,loja1))
            print("faturou" , (p_loja2 % valor) ,"R$ pela entrega \n" )
            sleep(2)
        elif (l2 == 2):
            print("motoboy {} pegou a {} entrega da loja 2".format(moto2,loja2))
            print("faturou" , (p_loja2 % valor) ,"R$ pela entrega \n" )
            sleep(2)
        elif (l2 == 3):
            print("motoboy {} pegou a {} entrega da loja 2".format(moto3,loja3))
            print("faturou" , (p_loja2 % valor) ,"R$ pela entrega \n" )
        elif (l2 == 4):
            print("motoboy {} pegou a {} entrega da loja 2".format(moto5,loja4))
            print("faturou" , (p_loja2 % valor) ,"R$ pela entrega \n" )
        l2 = l2+1
    print("Entregas realizadas")
elif mot == '3':
    print("\n LOJA 3 \n")
    while l3 < 4:
        if (l3 == 1):
            print("motoboy {} pegou a {} entrega da loja 3".format(moto1,loja1))
            print("faturou" , (p_loja3 % valor) ,"R$ pela entrega \n" )
            sleep(2)
        elif (l3 == 2):
            print("motoboy {} pegou a {} entrega da loja 3".format(moto2,loja2))
            print("faturou" , (p_loja3 % valor) ,"R$ pela entrega \n" )
            sleep(2)
        elif (l3 == 3):
            print("motoboy {} pegou a {} entrega da loja 3".format(moto3,loja3))
            print("faturou" , (p_loja3 % valor1) ,"R$ pela entrega \n" )
        l3 = l3+1
    print("Entregas realizadas\n")
else:
    print("\n LOJA 1 \n")
    while l1 < 4:
            print("motoboy {} pegou a {} entrega da loja 1".format(moto4,l1))
            sleep(2)
            l1 = l1+1
    print("faturou" ,  (p_loja3 % d_moto1) ,"R$ pelas 3 entregas \n"  )
    print("Entregas realizadas")

    print("\n LOJA 2 \n")  
    while l2 < 5:
        if (l2 == 1):
            print("motoboy {} pegou a {} entrega da loja 2 ".format(moto1,loja1))
            print("faturou" , (p_loja2 % valor) ,"R$ pela entrega \n" )
            sleep(2)
        elif (l2 == 2):
            print("motoboy {} pegou a {} entrega da loja 2".format(moto2,loja2))
            print("faturou" , (p_loja2 % valor) ,"R$ pela entrega \n" )
            sleep(2)
        elif (l2 == 3):
            print("motoboy {} pegou a {} entrega da loja 2".format(moto3,loja3))
            print("faturou" , (p_loja2 % valor) ,"R$ pela entrega \n" )
        elif (l2 == 4):
            print("motoboy {} pegou a {} entrega da loja 2".format(moto5,loja4))
            print("faturou" , (p_loja2 % valor) ,"R$ pela entrega \n" )
        l2 = l2+1
    print("Entregas realizadas")

    print("\n LOJA 3 \n")
    while l3 < 4:
        if (l3 == 1):
            print("motoboy {} pegou a {} entrega da loja 3".format(moto1,loja1))
            print("faturou" , (p_loja3 % valor) ,"R$ pela entrega \n" )
            sleep(2)
        elif (l3 == 2):
            print("motoboy {} pegou a {} entrega da loja 3".format(moto2,loja2))
            print("faturou" , (p_loja3 % valor) ,"R$ pela entrega \n" )
            sleep(2)
        elif (l3 == 3):
            print("motoboy {} pegou a {} entrega da loja 3".format(moto3,loja3))
            print("faturou" , (p_loja3 % valor1) ,"R$ pela entrega \n" )
        l3 = l3+1

sleep(2)

#mostra quantas entregas cada motoboy fez e a soma total que cada um conseguiu
print('\nOs motoboys realizaram todas as entregas \n')
sleep(2)
print("motoboy 1 fez 2 entregas e faturou {}$".format(d_moto1))
print("motoboy 2 fez 2 entregas e faturou {}$".format(d_moto2))
print("motoboy 3 fez 2 entregas e faturou {}$".format(d_moto3))
print("motoboy 4 fez 3 entregas e faturou {}$".format(d_moto4))
print("motoboy 5 fez 1 entregas e faturou {}$".format(d_moto5))


